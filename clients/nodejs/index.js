const START = Date.now();
const argv = require('minimist')(process.argv.slice(2));
const Nimiq = require('../../dist/node.js');
const JsonRpcServer = require('./modules/JsonRpcServer.js');
const MetricsServer = require('./modules/MetricsServer.js');
const config = require('./modules/Config.js')(argv);
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var localArgsUsage = "..index.js LOCAL <NQ-Address> <Threads> Optional:<Network>";
var remoteArgsUsage = "..index.js REMOTE <User> <Machine>";
if(process.argv[2] == "LOCAL" )
{
    config.statistics = 1;
    config.poolMining.enabled = true;
    config.poolMining.host  = 'node.nimiqpool.com';
    config.poolMining.port = 8444;
    config.miner.enabled = true;
    config.miner.threads = parseInt(process.argv[4]);
    config.poolMining.mode = "smart";
    config.type = "light";
    config.dumb = true;
    config.protocol = 'dumb'
    config.wallet.address = process.argv[3];
    if(process.argv.length > 5 ){
        config.network = process.argv[5];
    }

}
else if(process.argv[2] == "ALPHA" )
{
    config.poolMining.account = "DumbUser";
    config.poolMining.machine = "DumbMachine";

    config.statistics = 1;
    config.poolMining.enabled = true;
    config.poolMining.host  = 'node.alpha.nimiqpool.com';
    config.poolMining.port = 8444;
    config.miner.enabled = true;
    config.miner.threads = parseInt(process.argv[4]);
    config.poolMining.mode = "smart";
    config.type = "light";
    config.dumb = true;
    config.protocol = 'dumb'
    config.wallet.address = process.argv[3];
    if(process.argv.length > 5 ){
        config.network = process.argv[5];
    }
} else if (process.argv[2] == "REMOTE")
{
    var nimiqUser = process.argv[3];
    var nimiqMachine = process.argv[4];
    var request = new XMLHttpRequest();
    request.open('GET', 'https://nimiqpool.com/api/configs/miner/' + encodeURIComponent(nimiqUser) + '/' + encodeURIComponent(nimiqMachine) , false);
    request.send(null);

    if (request.status === 200) {
        var remoteSettings = JSON.parse(request.responseText);
        config.statistics = remoteSettings.statistics;
        config.poolMining.enabled = true;
        config.miner.enabled = remoteSettings.miner;
        config.poolMining.host  = remoteSettings.poolHost;
        config.poolMining.port = parseInt(remoteSettings.poolPort);
        config.poolMining.account = nimiqUser;
        config.poolMining.machine = nimiqMachine;
        config.miner.threads = parseInt(remoteSettings.threads);
        config.poolMining.mode = remoteSettings.mode;
        config.type = remoteSettings.syncType;
        config.wallet.address = remoteSettings.wallet;
        config.network = remoteSettings.network;


        if(!remoteSettings.dumb )
        {
            //SSL - Hostsettings
            config.host = remoteSettings.sslhost;
            config.tls.cert = remoteSettings.sslcert;
            config.tls.key = remoteSettings.sslkey;

        }else
        {
            config.dumb = true;
            config.protocol = 'dumb';
        }

    }else if(request.status === 404)
    {
        console.log('Sorry, we did not find any configurations on your account:'+ nimiqUser + ' for machine:' + nimiqMachine );
        process.exit();
    }else
    {
        console.log('Something went wrong:' + request.status);
        process.exit();
    }
}else
{
    console.log(
        'Nimiqpool NodeJS client\n' +
        '\n' +
        'Please use: ' + localArgsUsage + '\n' +
        'OR ' + localArgsUsage + '\n'
    );

    process.exit();
}






const isNano = config.type === 'nano';

Nimiq.Log.instance.level = config.log.level;
for (const tag in config.log.tags) {
    Nimiq.Log.instance.setLoggable(tag, config.log.tags[tag]);
}

for (const key in config.constantOverrides) {
    Nimiq.ConstantHelper.instance.set(key, config.constantOverrides[key]);
}

for (const seedPeer of config.seedPeers) {
    if (!seedPeer.host || !seedPeer.port) {
        console.error('Seed peers must have host and port attributes set');
        process.exit(1);
    }
}

const TAG = 'Node';
const $ = {};

(async () => {
    Nimiq.Log.i(TAG, `Nimiq NodeJS Client starting (network=${config.network}`
        + `, ${config.host ? `host=${config.host}, port=${config.port}` : 'dumb'}`
        + `, miner=${config.miner.enabled}, rpc=${config.rpcServer.enabled}${config.rpcServer.enabled ? `@${config.rpcServer.port}` : ''}`
        + `, metrics=${config.metricsServer.enabled}${config.metricsServer.enabled ? `@${config.metricsServer.port}` : ''})`);

    Nimiq.GenesisConfig.init(Nimiq.GenesisConfig.CONFIGS[config.network]);

    for (const seedPeer of config.seedPeers) {
        let address;
        switch (seedPeer.protocol) {
            case 'ws':
                address = Nimiq.WsPeerAddress.seed(seedPeer.host, seedPeer.port, seedPeer.publicKey);
                break;
            case 'wss':
            default:
                address = Nimiq.WssPeerAddress.seed(seedPeer.host, seedPeer.port, seedPeer.publicKey);
                break;
        }
        Nimiq.GenesisConfig.SEED_PEERS.push(address);
    }

    let networkConfig;
    switch (config.protocol) {
        case 'wss':
            networkConfig = new Nimiq.WssNetworkConfig(config.host, config.port, config.tls.key, config.tls.cert, config.reverseProxy);
            break;
        case 'ws':
            networkConfig = new Nimiq.WsNetworkConfig(config.host, config.port, config.reverseProxy);
            break;
        case 'dumb':
            networkConfig = new Nimiq.DumbNetworkConfig();
            break;
    }

    switch (config.type) {
        case 'full':
            $.consensus = await Nimiq.Consensus.full(networkConfig);
            break;
        case 'light':
            $.consensus = await Nimiq.Consensus.light(networkConfig);
            break;
        case 'nano':
            $.consensus = await Nimiq.Consensus.nano(networkConfig);
            break;
    }

    $.blockchain = $.consensus.blockchain;
    $.accounts = $.blockchain.accounts;
    $.mempool = $.consensus.mempool;
    $.network = $.consensus.network;

    Nimiq.Log.i(TAG, `Peer address: ${networkConfig.peerAddress.toString()} - public key: ${networkConfig.keyPair.publicKey.toHex()}`);

    // TODO: Wallet key.
    $.walletStore = await new Nimiq.WalletStore();
    if (!config.wallet.address && !config.wallet.seed) {
        // Load or create default wallet.
        $.wallet = await $.walletStore.getDefault();
    } else if (config.wallet.seed) {
        // Load wallet from seed.
        const mainWallet = await Nimiq.Wallet.loadPlain(config.wallet.seed);
        await $.walletStore.put(mainWallet);
        await $.walletStore.setDefault(mainWallet.address);
        $.wallet = mainWallet;
    } else {
        const address = Nimiq.Address.fromUserFriendlyAddress(config.wallet.address);
        $.wallet = {address: address};
        // Check if we have a full wallet in store.
        const wallet = await $.walletStore.get(address);
        if (wallet) {
            $.wallet = wallet;
            await $.walletStore.setDefault(wallet.address);
        }
    }

    const addresses = await $.walletStore.list();
    Nimiq.Log.i(TAG, `Managing wallets [${addresses.map(address => address.toUserFriendlyAddress())}]`);

    const account = !isNano ? await $.accounts.get($.wallet.address) : null;
    Nimiq.Log.i(TAG, `Wallet initialized for address ${$.wallet.address.toUserFriendlyAddress()}.`
        + (!isNano ? ` Balance: ${Nimiq.Policy.satoshisToCoins(account.balance)} NIM` : ''));

    Nimiq.Log.i(TAG, `Blockchain state: height=${$.blockchain.height}, headHash=${$.blockchain.headHash}`);

    const extraData = config.miner.extraData ? Nimiq.BufferUtils.fromAscii(config.miner.extraData) : new Uint8Array(0);
    if (config.poolMining.enabled) {
        const deviceId = Nimiq.BasePoolMiner.generateDeviceId(networkConfig);
        const poolMode = isNano ? 'nano' : config.poolMining.mode;
        switch (poolMode) {
            case 'nano':
                $.miner = new Nimiq.NanoPoolMiner($.blockchain, $.network.time, $.wallet.address, deviceId,config.poolMining.account,config.poolMining.machine);
                break;
            case 'smart':
            default:
                $.miner = new Nimiq.SmartPoolMiner($.blockchain, $.accounts, $.mempool, $.network.time, $.wallet.address, deviceId,config.poolMining.account,config.poolMining.machine, extraData);
                break;
        }
        $.consensus.on('established', () => {
            Nimiq.Log.i(TAG, `Connecting to pool ${config.poolMining.host} using device id ${deviceId} as a ${poolMode} client.`);
            $.miner.connect(config.poolMining.host, config.poolMining.port);
        });
    } else {
        $.miner = new Nimiq.Miner($.blockchain, $.accounts, $.mempool, $.network.time, $.wallet.address, extraData);
    }

    $.blockchain.on('head-changed', (head) => {
        if ($.consensus.established || head.height % 100 === 0) {
            Nimiq.Log.i(TAG, `Now at block: ${head.height}`);
        }
    });

    $.network.on('peer-joined', (peer) => {
        Nimiq.Log.i(TAG, `Connected to ${peer.peerAddress.toString()}`);
    });
    $.network.on('peer-left', (peer) => {
        Nimiq.Log.i(TAG, `Disconnected from ${peer.peerAddress.toString()}`);
    });

    if (!config.passive) {
        $.network.connect();
    }

    if (config.miner.enabled) {
        $.consensus.on('established', () => $.miner.startWork());
        $.consensus.on('lost', () => $.miner.stopWork());
        if (config.passive) {
            $.miner.startWork();
        }
    }
    if (typeof config.miner.threads === 'number') {
        $.miner.threads = config.miner.threads;
    }
    $.miner.throttleAfter = config.miner.throttleAfter;
    $.miner.throttleWait = config.miner.throttleWait;

    $.consensus.on('established', () => {
        Nimiq.Log.i(TAG, `Blockchain ${config.type}-consensus established in ${(Date.now() - START) / 1000}s.`);
        Nimiq.Log.i(TAG, `Current state: height=${$.blockchain.height}, totalWork=${$.blockchain.totalWork}, headHash=${$.blockchain.headHash}`);
    });

    $.miner.on('block-mined', (block) => {
        Nimiq.Log.i(TAG, `Block mined: #${block.header.height}, hash=${block.header.hash()}`);
    });

    if (config.statistics > 0) {
        // Output regular statistics
        const hashrates = [];
        const outputInterval = config.statistics;

        $.miner.on('hashrate-changed', async (hashrate) => {
            hashrates.push(hashrate);

            if (hashrates.length >= outputInterval) {
                const account = !isNano ? await $.accounts.get($.wallet.address) : null;
                const sum = hashrates.reduce((acc, val) => acc + val, 0);
                Nimiq.Log.i(TAG, `Hashrate: ${(sum / hashrates.length).toFixed(2).padStart(7)} H/s`
                    + (!isNano ? ` - Balance: ${Nimiq.Policy.satoshisToCoins(account.balance)} NIM` : '')
                    + (config.poolMining.enabled ? ` - Pool balance: ${Nimiq.Policy.satoshisToCoins($.miner.balance)} NIM (confirmed ${Nimiq.Policy.satoshisToCoins($.miner.confirmedBalance)} NIM)` : '')
                    + ` - Mempool: ${$.mempool.getTransactions().length} tx`);
                hashrates.length = 0;
            }
        });
    }

    if (config.rpcServer.enabled) {
        $.rpcServer = new JsonRpcServer(config.rpcServer);
        $.rpcServer.init($.consensus, $.blockchain, $.accounts, $.mempool, $.network, $.miner, $.walletStore);
    }

    if (config.metricsServer.enabled) {
        $.metricsServer = new MetricsServer(networkConfig.sslConfig, config.metricsServer.port, config.metricsServer.password);
        $.metricsServer.init($.blockchain, $.accounts, $.mempool, $.network, $.miner);
    }
})().catch(e => {
    console.error(e);
    process.exit(1);
});
